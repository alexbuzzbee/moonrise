--[[
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
]]--

function main()
  APP_NAME = "moonrise"
  package.path = package.path .. ";/usr/share/moonrise/?.lua"
  
  local async = require("async")
  local dispatch = require("dispatch")

  -- TODO: Do some real work.
  dispatch.run(function()
      local status = async.await(moonrise.run_cmd("/bin/echo", "This was run asynchronously from within Moonrise."))
      print("Command returned status " .. status)
  end)
  
  dispatch.dispatch_forever()
end

local success, reason = pcall(main)

if not success then
  print(APP_NAME .. ": (fatal) " .. reason)
end
