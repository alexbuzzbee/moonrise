--[[
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
]]--

local async = {}

-- Yield control until the asynchronous operation completes, then return the result or throw the error.
function async.await(op)
  if not type(op.is_complete) == "function" then
    error("can only await asynchronous operations")
  end
  
  while not op:is_complete() do
    coroutine.yield("waiting_for", op)
  end

  if op:error() ~= nil then
    error(op:error())
  else
    return op:result()
  end
end

-- Create a pure-Lua asynchronous operation. The operation can be completed by passing the result to its succeed method or the error to it fail method.
function async.new()
  local o = {}

  function o.is_complete()
    return o._complete
  end

  function o.wait_for_complete()
    pcall(async.await(o))
  end

  function o.result()
    return o._result
  end

  function o.error()
    return o._error
  end

  function o.succeed(_, res)
    o._complete = true
    o._result = res
  end

  function o.fail(_, err)
    o._complete = true
    o._error = err
  end

  o._complete = false

  return o
end

return async
