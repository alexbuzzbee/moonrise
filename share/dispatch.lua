--[[
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
]]--

local dispatch = {}

local ready_tasks = {}

local work_sources = {}

-- When waiting for an asynchronous operation, a task is kept in this table. When the operation completes, the task becomes ready again.
-- Awaiting tasks are stored as pairs of task: op where op is the operation being waited for.
local async_awaiting_tasks = {}

function work_sources.async_awaiting()
  local tasks = {}
  for task, op in pairs(async_awaiting_tasks) do
    if type(op.is_complete) == "function" and op:is_complete() then
      table.insert(tasks, task)
      async_awaiting_tasks[task] = nil
    end
  end
  return tasks
end

-- Add newly-ready tasks to the ready tasks list.
function dispatch.collect_work()
  for _, source in pairs(work_sources) do
    for _, task in pairs(source()) do
      table.insert(ready_tasks, task)
    end
  end
end

-- Register a work source
function dispatch.register_work_source(name, source)
  checkarg(name, "string")
  checkarg(source, "function")
  if work_sources[name] ~= nil then
    error("duplicate work source")
  end
  work_sources[name] = source
end

-- Run all ready tasks and rebuild the ready task list, removing any tasks that have finished. Returns true when there is work to do.
function dispatch.dispatch()
  -- Empty ready list.
  local to_run = ready_tasks;
  ready_tasks = {}

  for _, task in pairs(to_run) do
    local success, reason, p1 = coroutine.resume(task)
    
    if not success then
      print(APP_NAME + ": " .. reason)
    elseif reason == nil and not coroutine.status(task) == "dead" then -- Yielding nil indicates that the task is either dead or just offering to give up the thread.
      table.insert(ready_tasks, task)
    elseif reason == "waiting_for" then
      async_awaiting_tasks[task] = p1
    end
  end
  
  dispatch.collect_work()
  
  if #ready_tasks == 0 then
    return false
  else
    return true
  end
end

-- Dispatch work forever, sleeping when there is nothing to do.
function dispatch.dispatch_forever()
  while true do
    -- Reduce wasted CPU time when there is no work to do.
    if not dispatch.dispatch() then
      os.execute("sleep 1") -- FIXME: This is a terrible way to handle sleeping.
    end
  end
end

-- Convert a function into a task and schedule it to run.
function dispatch.run(func)
  table.insert(ready_tasks, coroutine.create(func))
end

return dispatch
