#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Installs Moonrise.
# install.sh level
# Level is debug or release.

cd `dirname $0`/..

install -g bin -u root -d /usr/share/moonrise
install -g bin -u root ./share/* /usr/share/moonrise
install -g bin -u root ./target/$1/moonrise /sbin
echo "Add init=/sbin/moonrise to kernel command line, or symlink /sbin/init to /sbin/moonrise if you are very confident."
