//! API calls for managing processes.

/*
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

/// A running process started asynchronously.
pub struct Process {
    child: std::process::Child,
    status: Option<std::process::ExitStatus>,
    wait_failed: bool,
    done: bool,
}

impl super::Await<i32> for Process {
    fn is_complete(&mut self) -> bool {
        if self.done {
            return true;
        }
        
        match self.child.try_wait() {
            Ok(o) => {
                match o {
                    Some(s) => {
                        self.status = Some(s);
                        self.done = true;
                        true
                    },
                    None => false
                }
            },
            Err(_) => {
                self.wait_failed = true;
                true
            },
        }
    }

    fn wait_for_complete(&mut self) {
        if self.done {
            return;
        }
        
        match self.child.wait() {
            Ok(s) => {
                self.status = Some(s);
            },
            Err(_) => {
                self.wait_failed = true;
            },
        }
    }

    fn result(&mut self) -> Option<i32> {
        if self.wait_failed {
            return None;
        }

        match self.status {
            Some(s) => {
                match s.code() {
                    Some(n) => {
                        Some(n)
                    },
                    None => {
                        None
                    },
                }
            },
            None => {
                None
            },
        }
    }

    fn error(&mut self) -> Option<&str> {
        if self.wait_failed {
            return Some("wait failed");
        }

        match self.status {
            Some(_) => {
                None
            },
            None => {
                Some("incomplete")
            }
        }
        
    }
}

/// Lua API function.
/// Takes any number of arguments and uses them to execute an arbitrary program. The first argument is both `argv[0]` and the program path.
/// Returns an asynchronous operation resolving to a number, or `nil` in case of error.
/// Similar to an async `os.execute()`.
pub fn run_cmd(state: &mut luajit::State) -> i32 {
    let name = match state.to_str(1) {
        Some(s) => s,
        None => {return 0;},
    };
    let mut cmd: std::process::Command = std::process::Command::new(name);

    let mut pos = 2;
    while state.is_string(pos) {
        let arg = match state.to_str(pos) {
            Some(s) => s,
            None => {return 0;},
        };
        cmd.arg(arg);

        pos += 1;
    }

    match cmd.spawn() {
        Ok(c) => {
            let mut proc = Box::<Process>::new(Process {
                child: c,
                status: None,
                wait_failed: false,
                done: false,
            });
            state.push::<super::LuaOperation::<i32>>(super::LuaOperation::<i32>(proc.as_mut()));
            1
        },
        Err(_) => {
            0
        }
    }   
}
