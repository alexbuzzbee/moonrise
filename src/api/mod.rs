//! The Moonrise API, exposed to Lua code run by Moonrise.
//! This API is mostly asynchronous; Lua code can call an API function and get a userdata that can be tested for completion or blocked on.

/*
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

/// An operation that can be waited for. Automatically bridged to Lua via `LuaOperation`.
pub trait Await<T: luajit::types::LuaValue> {
    /// Returns whether the operation has completed yet.
    fn is_complete(&mut self) -> bool;

    /// Blocks until the operation to complete, then returns.
    fn wait_for_complete(&mut self) -> ();
    
    /// Returns the result of the operation, if any.
    /// If the operation was unsuccessful, the result should be None. This will be converted to `nil`.
    fn result(&mut self) -> Option<T>;

    /// Returns the error that prevented completion of the operation, if any.
    /// If the operation was successful, the error should be None. This will be converted to `nil`.
    /// If the operation has not yet completed, the error should be the string "incomplete".
    fn error(&mut self) -> Option<&str>;
}

/// Bridges `Await` to Lua.
struct LuaOperation<'a, T: luajit::types::LuaValue>(&'a mut dyn Await<T>);

// These are methods for LuaOperation.

fn is_complete<T: luajit::types::LuaValue>(this: &mut LuaOperation<T>, state: &mut luajit::State) -> i32 {
    state.push::<bool>(this.0.is_complete());
    1
}

fn wait_for_complete<T: luajit::types::LuaValue>(this: &mut LuaOperation<T>, state: &mut luajit::State) -> i32 {
    this.0.wait_for_complete();
    0
}

fn result<T: luajit::types::LuaValue>(this: &mut LuaOperation<T>, state: &mut luajit::State) -> i32 {
    match this.0.result() {
        Some(r) => {
            state.push::<T>(r);
        },
        None => {
            state.push_nil();
        },
    };
    1
}

fn error<T: luajit::types::LuaValue>(this: &mut LuaOperation<T>, state: &mut luajit::State) -> i32 {
    match this.0.error() {
        Some(r) => {
            state.push::<&str>(r);
        },
        None => {
            state.push_nil();
        },
    };
    1
}

// Partial manual expansion of lua_method! because it doesn't work with LuaOperation<T>. Sorry.
unsafe extern "C" fn is_complete_trampoline<U: luajit::types::LuaValue>(l: *mut luajit::ffi::lua_State) -> luajit::c_int {
    let mut state = luajit::State::from_ptr(l);
    let st = &mut *state.check_userdata::<LuaOperation<U>>(1).unwrap();
    
    is_complete::<U>(st, &mut state)
}

unsafe extern "C" fn wait_for_complete_trampoline<U: luajit::types::LuaValue>(l: *mut luajit::ffi::lua_State) -> luajit::c_int {
    let mut state = luajit::State::from_ptr(l);
    let st = &mut *state.check_userdata::<LuaOperation<U>>(1).unwrap();
    
    wait_for_complete::<U>(st, &mut state)
}

unsafe extern "C" fn result_trampoline<U: luajit::types::LuaValue>(l: *mut luajit::ffi::lua_State) -> luajit::c_int {
    let mut state = luajit::State::from_ptr(l);
    let st = &mut *state.check_userdata::<LuaOperation<U>>(1).unwrap();
    
    result::<U>(st, &mut state)
}

unsafe extern "C" fn error_trampoline<U: luajit::types::LuaValue>(l: *mut luajit::ffi::lua_State) -> luajit::c_int {
    let mut state = luajit::State::from_ptr(l);
    let st = &mut *state.check_userdata::<LuaOperation<U>>(1).unwrap();
    
    error::<U>(st, &mut state)
}

impl<'a, T: luajit::types::LuaValue> luajit::LuaObject for LuaOperation<'a, T> {
    fn name() -> *const i8 {
        c_str!("moonrise_LuaOperation")
    }

    fn lua_fns() -> Vec<luajit::ffi::lauxlib::luaL_Reg> {
        // More manual expansion of lua_method!.
        vec!(
            luajit::ffi::lauxlib::luaL_Reg {
                name: c_str!("is_complete"),
                func: Some(is_complete_trampoline::<T>),
            },
            luajit::ffi::lauxlib::luaL_Reg {
                name: c_str!("wait_for_complete"),
                func: Some(wait_for_complete_trampoline::<T>),
            },
            luajit::ffi::lauxlib::luaL_Reg {
                name: c_str!("result"),
                func: Some(result_trampoline::<T>),
            },
            luajit::ffi::lauxlib::luaL_Reg {
                name: c_str!("error"),
                func: Some(error_trampoline::<T>),
            }
        )
    }
}

mod process;

pub fn register(state: &mut luajit::State) {
  state.register_fns(Some("moonrise"), vec!(lua_func!("run_cmd", process::run_cmd)));
}
