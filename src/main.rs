/// Moonrise, a Linux (possibly other Unix-likes as well) init system based on the Lua scripting language.

/*
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#[macro_use]
extern crate luajit;

mod api;

pub fn main() {
    let mut state = luajit::State::new();
    state.open_libs();
    
    api::register(&mut state);
    
    // TODO: Do PID1 things (e.g. cd /, reap orphan zombies, handle signals).
    
    match state.do_file(std::path::Path::new("/usr/share/moonrise/main.lua")) {
        Ok(_) => {println!("moonrise: main.lua exited");},
        Err(_) => {println!("moonrise: main.lua failed to run");},
    };
}
