# Moonrise

Moonrise is a Linux init system written in Lua with Rust support code. An init system is a software suite responsible for bringing the userspace components of an operating system online and, in most cases, managing long-running components such as background services.

Moonrise is broken into several pieces. A Rust binary crate, called `moonrise`, provides the host program that performs essential PID 1 tasks and loads and runs the Lua components. Also part of the `moonrise` crate is an asynchronous API that allows Lua code to perform many tasks concurrently. The core of the Lua components is a Lua coroutine dispatcher that integrates with a small asynchronicity framework that allows tasks to await asynchronous operations and let other tasks run. Around this core, a framework of system and service management components is provided that helps Moonrise perform the work of an init system. Finally, a library of optional "init modules" does the actual system and service management using the other components.

## Current state

Moonrise is currently incomplete, unstable, and not suitable for deployment in any environment except for development testing environments.

## Building and running

To build Moonrise, make sure you have Rust, Cargo, and LuaJIT installed and run `cargo build` in the Moonrise root directory. Moonrise requires its Lua components to be installed at `/usr/share/moonrise`; create this directory, copy the contents of `share/` there and make sure they are readable by you.

To run Moonrise, having built it previously, run `target/debug/moonrise` or `cargo run`. Currently, Moonrise will use `/bin/echo` to display a message, then display `echo`'s exit status and sleep forever.

Currently, terminating Moonrise is difficult because it uses `/bin/sleep` to sleep (this will be fixed later); press ^Z several times quickly, then use shell job control to kill the Moonrise job.
