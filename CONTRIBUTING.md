# Contributing to Moonrise

Feel free to submit issues that you find in Moonrise via the GitLab issues system.

If there's an issue you want to work on, or just a feature you want to add or a bug you want to fix, you have two options:

1. Fork the project via GitLab if you have an account, make your changes in your fork, and open a merge request detailing your changes.
2. Clone the project, make your changes locally, and email patches to `alexbuzzbee@gmail.com` using [Git's built-in tools for email patches](https://www.git-scm.com/book/en/v2/Appendix-C%3A-Git-Commands-Email).